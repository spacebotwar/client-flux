var React       = require('react');
var actions     = require('./actions.js');

var LoginForm = React.createClass({

    handleLoginSubmit: function (event) {
        event.preventDefault();
        var username = this.refs.username.getDOMNode();
        var password = this.refs.password.getDOMNode();
        actions.loginWithPassword(username.value, password.value)
    },

    render: function () {
        return (
            <div>
              <form onSubmit={this.handleLoginSubmit}>
                <div><input placeholder="User Name" ref="username" type="text" /></div>
                <div><input placeholder="Password" ref="password" type="password" /></div>
                <div><input type="submit" value="Submit" /></div>
              </form>
            </div>
        );
    }
});
	
module.exports = LoginForm;
