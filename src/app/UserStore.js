var flux = require('flux-react');
var actions = require('./actions.js');

var UserStore = flux.createStore({
    user: {
        // enum states NOT_LOGGED_IN, LOGGED_IN, FORGOT_PASSWORD, REGISTERING
        state:      'NOT_LOGGED_IN',
        username:   '',
        email:      '',
        password:   ''
    },
    actions: [
        actions.loginWithPassword,
        actions.userLogin,
        actions.userForgotPassword,
        actions.userLogout,
        actions.changeUsername
    ],
    loginWithPassword: function (username, password) {
        this.user.username = username;
        this.user.password = password;
        this.user.state = 'LOGGED_IN';
        this.emitChange();
        console.log("Login With Username: username="+username+" password="+password);
    },
    userLogin: function (username, password) {
        this.user.username = username;
        this.user.password = password;
        this.user.state = 'LOGGED_IN';
        this.emitChange();
    },
    userForgotPassword: function () {
        this.user.username = username;
        this.user.state = 'NOT_LOGGED_IN';
        this.emitChange();
    },
    userLogout: function () {
        this.user.username = '';
        this.user.email = '';
        this.user.password = '';
        this.user.state = 'NOT_LOGGED_IN';
        this.emitChange();
    },
    changeUsername: function (username) {
        this.user.username = username;
        this.emitChange();
    },
    exports: {
        getUser: function () {
            return this.user;
        }
    }
});

module.exports = UserStore;
