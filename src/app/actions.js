var flux = require('flux-react');
flux.debug();

module.exports = flux.createActions([
    'loginWithPassword',
    'userLogin',
    'userForgotPassword',
    'userLogout',
    'changeUsername'
]);
