var React       = require('react');
var UserStore   = require('./UserStore.js');
var actions     = require('./actions.js');
var LoginForm   = require('./LoginForm.js');

function getUserState() {
    return {
        user:   UserStore.getUser()
    };
}

var App = React.createClass({
    getInitialState: function () {
        return getUserState();
    },
    componentWillMount: function () {
        UserStore.addChangeListener(this._onChange);
    },
    componentWillUnmount: function () {
        UserStore.removeChangeListener(this._onChange);
    },
    _onChange: function () {
        this.setState(getUserState());
    },
    updateUser: function () {
        var input = this.refs.username.getDOMNode();

    },
    render: function() {
        return (
            <div>
              <p>Hello World</p>
              <LoginForm />
            </div>
        );
    }

});
	
module.exports = App;
