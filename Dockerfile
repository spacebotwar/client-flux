FROM sbw-client:latest


RUN npm install -g gulp
RUN npm install -g http-server
EXPOSE 8888

WORKDIR /src
CMD "gulp"



#FROM ubuntu:latest

#RUN apt-get install curl
#RUN curl -sL https://deb.nodesource.com/setup_4.x | bash -
#RUN apt-get install -y nodejs npm
#
#COPY ${PWD}/src/package.json /src/package.json
#RUN cd /src; 
#RUN npm install
#
#COPY . /src
#
#EXPOSE 8888
##CMD ["node", "/src/index.js"]
